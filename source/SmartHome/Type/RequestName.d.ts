// @formatter:off
declare const enum RequestName {
  ReportState = 'ReportState',
  ChangeReport = 'ChangeReport',
  DeferredResponse = 'DeferredResponse',
  ErrorResponse = 'ErrorResponse',
  Response = 'Response',
  StateReport = 'StateReport',

  Alexa = 'Alexa',
  Discovery = 'Alexa.Discovery',
  Authorization= 'Alexa.Authorization',
  BrightnessController = 'Alexa.BrightnessController',
  CameraStreamController = 'Alexa.CameraStreamController',
  ChannelController = 'Alexa.ChannelController',
  ColorController = 'Alexa.ColorController',
  ColorTemperatureController = 'Alexa.ColorTemperatureController',
  EndpointHealth = 'Alexa.EndpointHealth',
  InputController = 'Alexa.InputController',
  LockController = 'Alexa.LockController',
  PercentageController = 'Alexa.PercentageController',
  PlaybackController = 'Alexa.PlaybackController',
  PowerController = 'Alexa.PowerController',
  PowerLevelController = 'Alexa.PowerLevelController',
  SceneController = 'Alexa.SceneController',
  Speaker = 'Alexa.Speaker',
  StepSpeaker = 'Alexa.StepSpeaker',
  TemperatureSensor = 'Alexa.TemperatureSensor',
  ThermostatController = 'Alexa.ThermostatController',

  // Control = 'Alexa.ConnectedHome.Control',
  // Query = 'Alexa.ConnectedHome.Query'
}
// @formatter:on

export default RequestName;
