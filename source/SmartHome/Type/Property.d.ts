export default interface Property {
  name: string;
  namespace: string;
  timeOfSample: string;
  uncertaintyInMilliseconds: number;
  value?: any;
}
