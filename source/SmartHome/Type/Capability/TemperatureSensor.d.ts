import Endpoint from '../Endpoint';
import Header from '../Header';
import Property from '../Property';
import {TemperatureValue} from './ThermostatController';

export interface TemperatureSensorRequest {
  directive: { header: Header; endpoint: Endpoint; payload: {}; };
}

export interface TemperatureSensorProperty extends Property {
  temperature: TemperatureValue,
}

export interface TemperatureSensorReponse {
  context: { properties: TemperatureSensorProperty[]; };
  event: { header: Header; endpoint: Endpoint; payload: {}; };
}
