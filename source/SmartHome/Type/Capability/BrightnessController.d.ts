import Endpoint from '../Endpoint';
import Header from '../Header';
import Property from '../Property';

export interface BrightnessControllerRequest {
  directive: { header: Header; endpoint: Endpoint; payload: {}; };
}

export interface BrightnessControllerProperty extends Property {
  value: number;
}

export interface BrightnessControllerReponse {
  context: { properties: BrightnessControllerProperty[]; };
  event: { header: Header; endpoint: Endpoint; payload: {}; };
}
