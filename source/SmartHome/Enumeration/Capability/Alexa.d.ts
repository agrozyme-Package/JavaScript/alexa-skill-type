// @formatter:off

declare const enum Alexa {
  ReportState = 'ReportState',
  StateReport = 'StateReport'
}
// @formatter:on

export default Alexa;
