/** @link https://developer.amazon.com/docs/device-apis/alexa-property-schemas.html#foodcount-size */

// @formatter:off
declare const enum FoodSize {
  EXTRA_EXTRA_LARGE = 'EXTRA_EXTRA_LARGE',
  JUMBO = 'JUMBO',
  EXTRA_LARGE = 'EXTRA_LARGE',
  LARGE = 'LARGE',
  MEDIUM = 'MEDIUM',
  SMALL = 'SMALL',
  EXTRA_SMALL = 'EXTRA_SMALL',
  SIZE_A = 'SIZE_A',
  SIZE_B = 'SIZE_B',
}
// @formatter:on

export default FoodSize;
