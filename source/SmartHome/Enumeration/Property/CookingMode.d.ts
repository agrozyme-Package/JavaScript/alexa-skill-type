/** @link https://developer.amazon.com/docs/device-apis/alexa-property-schemas.html#cookingmode */

// @formatter:off
declare const enum CookingMode {
  DEFROST = 'DEFROST',
  OFF = 'OFF',
  PRESET = 'PRESET',
  REHEAT = 'REHEAT',
  TIMECOOK = 'TIMECOOK',
}
// @formatter:on

export default CookingMode;
