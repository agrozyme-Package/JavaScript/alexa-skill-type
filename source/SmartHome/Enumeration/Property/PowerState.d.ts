/** @link https://developer.amazon.com/docs/device-apis/alexa-property-schemas.html#powerstate */

// @formatter:off
declare const enum PowerState {
  ON = 'ON',
  OFF = 'OFF',
}
// @formatter:on

export default PowerState;
