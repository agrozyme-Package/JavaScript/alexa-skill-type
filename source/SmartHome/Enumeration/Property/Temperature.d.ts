/** @link https://developer.amazon.com/docs/device-apis/alexa-property-schemas.html#temperature */

// @formatter:off
declare const enum Temperature {
  CELSIUS = 'CELSIUS',
  FAHRENHEIT = 'FAHRENHEIT',
  KELVIN = 'KELVIN',
}
// @formatter:on

export default Temperature;
