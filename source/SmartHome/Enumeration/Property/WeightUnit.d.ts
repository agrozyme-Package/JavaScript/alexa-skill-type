/** @link https://developer.amazon.com/docs/device-apis/alexa-property-schemas.html#weight-unit */

// @formatter:off
declare const enum WeightUnit {
  GRAM = 'GRAM',
  KILOGRAM = 'KILOGRAM',
  OUNCE = 'OUNCE',
  POUND = 'POUND',

  METRIC_POUND = 'METRIC_POUND',
  MICROGRAM = 'MICROGRAM',
  MILLIGRAM = 'MILLIGRAM',
}
// @formatter:on

export default WeightUnit;
