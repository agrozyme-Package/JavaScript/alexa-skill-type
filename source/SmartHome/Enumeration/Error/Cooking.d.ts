/** @link https://developer.amazon.com/docs/device-apis/alexa-cooking-errorresponse.html#error-type-values */

// @formatter:off
declare const enum Cooking {
  DOOR_OPEN = 'DOOR_OPEN',
  DOOR_CLOSED_TOO_LONG = 'DOOR_CLOSED_TOO_LONG',
  COOK_DURATION_TOO_LONG = 'COOK_DURATION_TOO_LONG',
  REMOTE_START_NOT_SUPPORTED = 'REMOTE_START_NOT_SUPPORTED',
  REMOTE_START_DISABLED = 'REMOTE_START_DISABLED',
}
// @formatter:on

export default Cooking;
