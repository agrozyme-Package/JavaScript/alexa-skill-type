/** @link https://developer.amazon.com/docs/device-apis/alexa-discovery.html#display-categories */

// @formatter:off
declare const enum DisplayCategory {
  ACTIVITY_TRIGGER = 'ACTIVITY_TRIGGER',
  CAMERA = 'CAMERA',
  DOOR = 'DOOR',
  LIGHT = 'LIGHT',
  OTHER = 'OTHER',
  SCENE_TRIGGER = 'SCENE_TRIGGER',
  SMARTLOCK = 'SMARTLOCK',
  SMARTPLUG = 'SMARTPLUG',
  SPEAKER = 'SPEAKER',
  SWITCH = 'SWITCH',
  TEMPERATURE_SENSOR = 'TEMPERATURE_SENSOR',
  THERMOSTAT = 'THERMOSTAT',
  TV = 'TV',
}
// @formatter:on

export default DisplayCategory;
