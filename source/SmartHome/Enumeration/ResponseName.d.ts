// @formatter:off
declare const enum ResponseName {
  Discover = 'Discover.Response',
  Deferred = 'DeferredResponse',
  Error = 'ErrorResponse',
  Response = 'Response',
  ChangeReport = 'ChangeReport',
  StateReport = 'StateReport'
}
// @formatter:on

export default ResponseName;
