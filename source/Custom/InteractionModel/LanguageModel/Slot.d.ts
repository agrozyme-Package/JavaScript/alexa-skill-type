export default interface Slot {
  name: string;
  samples?: string[];
  type: string;
}
