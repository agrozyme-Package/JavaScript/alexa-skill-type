import Intent from './Intent';
import Type from './Type';

export default interface LanguageModel {
  intents: Intent[];
  invocationName: string;
  types?: Type[];
}
