export default interface Slot {
  confirmation?: string;
  confirmationRequired?: boolean;
  elicitation?: string;
  elicitationRequired?: boolean;
  name: string;
  prompts?: { elicitation?: string; confirmation?: string; };
  type: string;
}
