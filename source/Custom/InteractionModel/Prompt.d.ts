export default interface Prompt {
  id: string;
  variations: string[];
}
